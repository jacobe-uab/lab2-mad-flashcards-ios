//
//  Flashcard.swift
//  flashcards
//
//  Created by Jacob Ellis on 1/31/21.
//

import Foundation

class Flashcard {
    let term: String
    let definition: String
    
    init (term: String, definition: String) {
        self.term = term
        self.definition = definition
    }
    
    static func sampleCards() -> Array<Flashcard> {
        let terms = ["cpu", "gpu", "apu", "ram", "hdd", "ssd", "bios", "lan", "wan", "vpn"]
        let definitions = ["central processing unit", "graphics processing unit", "arithmetic processing unit", "random access memory", "hard disk drive", "solid state drive", "basic input/output system", "local area network", "wide area network", "virtual private network"]

        var flashcards = Array<Flashcard>()
        for i in 0...9 {
            flashcards.append(Flashcard(term: terms[i], definition: definitions[i]))
        }
        
        return flashcards
    }
}
