//
//  FlashcardSet.swift
//  flashcards
//
//  Created by Jacob Ellis on 1/31/21.
//

import Foundation

class FlashcardSet {
    let title: String
    
    init (title: String) {
        self.title = title
    }
    
    static func sampleCardSets() -> Array<FlashcardSet> {
        let titles = ["Algebra", "Calculus", "Biology", "Chemistry", "Physics", "English", "Spanish", "French", "US History", "World History"]
        
        let flashcardSets = titles.map { FlashcardSet(title: $0) }
        
        return flashcardSets
    }
}
