//
//  ViewController.swift
//  flashcards
//
//  Created by Jacob Ellis on 1/31/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Flashcard\n---------")
        let flashcards = Flashcard.sampleCards()
        
        for flashcard in flashcards {
            print("term: " + flashcard.term + ", definition: " + flashcard.definition)
        }
        
        print()
        print("FlashcardSet\n-----------")
        let flashcardSets = FlashcardSet.sampleCardSets()
        
        for flashcardSet in flashcardSets {
            print("title: " + flashcardSet.title)
        }
    }

}

